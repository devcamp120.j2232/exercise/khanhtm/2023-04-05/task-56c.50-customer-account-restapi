package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Customer;

@Service
public class CustomerService {
    Customer cus1 = new Customer(1, "Khanh", 10);
    Customer cus2 = new Customer(2, "Minh Khanh", 20);
    Customer cus3 = new Customer(3, "Trần Khanh", 30);

    public ArrayList<Customer> gCustomerList(){
        ArrayList<Customer> cusList = new ArrayList<>();

        cusList.add(cus1);
        cusList.add(cus2);
        cusList.add(cus3);

        return cusList ;
        
    }

}
